// No Copyright at the moment. Set It later on Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Components/InputComponent.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "Grabber.generated.h"  //generated.h harus di include paling terakhir


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDINGESCAPE_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrabber();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	// Setup (assumed) attached Input Component
	void FindSetupInputComponent();

	// Find (assumed) attached Physics Handle Component
	void FindPhysicsHandleComponent();

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	FVector GetLineTraceStart();

	FVector GetLineTraceEnd();

	const FHitResult GetFirstPhysicsBodyInReach();
	
private:
	const FColor LineTraceColor = FColor(25, 129, 208, 0.5f);
	const float Reach = 100.f;  // Ukuran panjang line trace

	UPhysicsHandleComponent* PhysicsHandle = nullptr;
	UInputComponent* InputComponent = nullptr;

	// Ray-cast and see what It's reach.
	void Grab();
	void Release();
};
