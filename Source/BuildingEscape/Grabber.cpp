// No Copyright at the moment. Set It later on Project Settings.


#include "Grabber.h"
#include "GameFramework/PlayerController.h"
#include "Engine/World.h"
#include "Engine/EngineTypes.h"
#include "DrawDebugHelpers.h"
#include "Components/PrimitiveComponent.h"

// OUT hanya berfungsi sebagai penjelas. digunakan untuk variabel berikut cth nya : out_Location, out_Rotation.
// Tidak memiliki pengaruh dalam code
#define OUT  

// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();
	FindPhysicsHandleComponent();
	FindSetupInputComponent();
}

/// Look for attached physics handler
void UGrabber::FindPhysicsHandleComponent()
{
	PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();  // Ekuivalen dg PhysicsHandle = gameObject.GetComponent<RigidBody>() di Unity3D
	InputComponent = GetOwner()->FindComponentByClass<UInputComponent>();
	if (PhysicsHandle == nullptr)
	{
		/// PhysicsHandle not found
		UE_LOG(LogTemp, Error, TEXT("%s component is not found: UPyhsicsHandle"), *GetOwner()->GetName());
	}
}

/// Look for attached input component
void UGrabber::FindSetupInputComponent()
{
	if (InputComponent != nullptr)
	{
		InputComponent->BindAction("Grab", IE_Pressed, this, &UGrabber::Grab);
		InputComponent->BindAction("Grab", IE_Released, this, &UGrabber::Release);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("%s component is not found: UInputComponent"), *GetOwner()->GetName())
	}
}

void UGrabber::Grab()
{
	UE_LOG(LogTemp, Warning, TEXT("Grab Pressed!"))

	/// LINE TRACE and reach any actors with physics body collision channel
	auto HitResult = GetFirstPhysicsBodyInReach();
	auto ComponentToGrab = HitResult.GetComponent();
	auto ActorHit = HitResult.GetActor();

	/// If we hit something
	if (ActorHit)
	{
		/// then attach a physics handle
		PhysicsHandle->GrabComponentAtLocationWithRotation(
			ComponentToGrab,
			NAME_None,  /// No bones needed
			ComponentToGrab->GetOwner()->GetActorLocation(),
			ComponentToGrab->GetOwner()->GetActorRotation()
		);
	}
}

void UGrabber::Release()
{
	PhysicsHandle->ReleaseComponent();
}

// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (PhysicsHandle == nullptr)
	{
		return;
	}
	/// If the physics handle is attached
	if (PhysicsHandle->GrabbedComponent)
	{
		/// move the object that we're holding
		PhysicsHandle->SetTargetLocation(GetLineTraceEnd());
	}
}

FVector UGrabber::GetLineTraceStart()
{
	FVector PlayerViewPointLocation;  /// Lokasi Player | returns (x, y, z)
	FRotator PlayerViewPointRotation;  /// Kemana player menghadap | variable to declare rotation

	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
		OUT PlayerViewPointLocation,
		OUT PlayerViewPointRotation
	);

	return PlayerViewPointLocation;  // Panjang Line Trace. (assumed) juga bisa menentukan jarak dari pawn dg object grab
}

FVector UGrabber::GetLineTraceEnd()
{
	FVector PlayerViewPointLocation;  /// Lokasi Player | returns (x, y, z)
	FRotator PlayerViewPointRotation;  /// Kemana player menghadap | variable to declare rotation

	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
		OUT PlayerViewPointLocation,
		OUT PlayerViewPointRotation
	);

	return PlayerViewPointLocation + PlayerViewPointRotation.Vector() * Reach;  // Panjang Line Trace. (assumed) juga bisa menentukan jarak dari pawn dg object grab
}

const FHitResult UGrabber::GetFirstPhysicsBodyInReach()
{
	/// Setup Query Parameters
	FCollisionQueryParams TraceParameters(FName(TEXT("")), false, GetOwner());  /// Set to true jika ingin trace collision secara visibility
																				/// Ray-cast out to reach distance.	

	FHitResult HitResult;  /// FHitResult declaration | berisi informasi mengenai sesuatu yang terkena hit oleh trace
	/// Hasil dari line trace akan return ke variable Hit
	GetWorld()->LineTraceSingleByObjectType(
		OUT HitResult,
		GetLineTraceStart(),
		GetLineTraceEnd(),
		FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody),
		TraceParameters
	);

	return HitResult;
}

