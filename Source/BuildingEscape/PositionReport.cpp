// Fill out your copyright notice in the Description page of Project Settings.


#include "PositionReport.h"
#include "GameFramework/Actor.h"

// Sets default values for this component's properties
UPositionReport::UPositionReport()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UPositionReport::BeginPlay()  // void Start()
{
	Super::BeginPlay();

	// GetOwner = Fungsi AActor yang memiliki return pointer (singkatnya, GetOwner adalah pointer). GetName = Fungsi untuk return nama dari object
	// FString ObjName adalah pointer. Jadi ketika akan melakukan output nilai dari ObjName, gunakan *ObjName untuk get nilai dari ObjName
	FString ObjName = GetOwner()->GetName();  // sama dengan : (*GetOwner()).GetName();
	FString ObjPos = GetOwner()->GetTransform().GetLocation().ToString();  // atau : GetOwner()->GetActorLocation().ToString()
	UE_LOG(LogTemp, Warning, TEXT("%s is at %s."), *ObjName, *ObjPos);  // TEXT() akan menghasilkan output dari alamat FString (array of char) ObjName
	
}


// Called every frame
void UPositionReport::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)  // void Update()
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

