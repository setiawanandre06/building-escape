// No Copyright at the moment. Set It later on Project Settings.


#include "OpenDoor.h"
#include "Components/ActorComponent.h"
#include "GameFramework/Actor.h"
#include "GameFramework/Controller.h"
#include "GameFramework/PlayerController.h"
#include "Components/PrimitiveComponent.h"
#include "Engine/World.h"

// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();

	// Pada saat game dimulai, set ActorThatOpens sebagai player utama
}

// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// Poll the trigger volume
	if (GetTotalMassOfActorsOnPlate() > TriggerMass)  // TODO : Make 50 a parameter
	{
		// If actor is in trigger volume
		OnOpen.Broadcast();
	}

	// Check if it's time to close the door
	else
	{
		//Close the door.
		OnClose.Broadcast();
	}
}

float UOpenDoor::GetTotalMassOfActorsOnPlate()
{
	float TotalMass = 0.0f;
	TArray<AActor*> ListOfOverlappingActors;

	// Finding overlapping actors
	if (PressurePlate == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Pressure Plate is Not Found!"));
		return 0;
	}
	PressurePlate->GetOverlappingActors(OUT ListOfOverlappingActors);

	// Iterate through them adding their masses
	for (const auto &OverlappingActor : ListOfOverlappingActors)
	{
		TotalMass += OverlappingActor->FindComponentByClass<UPrimitiveComponent>()->GetMass();
		UE_LOG(LogTemp, Warning, TEXT("Actor %s"), *OverlappingActor->GetName())
	}

	return TotalMass;
}
